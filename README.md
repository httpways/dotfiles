# dotfiles
My awesome dotfiles

## Prerequisites

```sh
sudo apt-get install git
mkdir -p ~/.ssh
ssh-keygen -b 4096 -N "" -f ~/.ssh/nick-laptop-ubuntu_rsa
cat ~/.ssh/nick-laptop-ubuntu_rsa.pub
```

## Install doing the following lines of code:

```sh
curl --silent https://bitbucket.org/httpways/dotfiles/raw/HEAD/setup.sh | sh
```

