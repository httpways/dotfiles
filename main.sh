#!/bin/bash

echo "
|---------------------------------------------|
|    ___  ___  _____  ________  __    __  __  |
|   /   \/___\/__   \/ __\_   \/ /   /__\/ _\ |
|  / /\ //  //  / /\/ _\  / /\/ /   /_\  \ \  |
| / /_// \_//  / / / / /\/ /_/ /___//__  _\ \ |
|/___,'\___/   \/  \/  \____/\____/\__/  \__/ |
|---------------------------------------------|
Instalation begins ...
"

if [ -d ~/.dotfiles ]; then
	echo "Deleting all existing dotfiles"
	rm -rfv ~/.dotfiles
	mkdir -p ~/.dotfiles
else
	echo "Creating the target directory"
	mkdir -p ~/.dotfiles
fi

git clone git@bitbucket.org:httpways/dotfiles.git ~/.dotfiles

sh ~/.dotfiles/.setup/git.sh
sh ~/.dotfiles/.setup/bitbucket.sh
sh ~/.dotfiles/.setup/installpkg.sh
sh ~/.dotfiles/.setup/bin.sh
sh ~/.dotfiles/.setup/i3.sh
sh ~/.dotfiles/.setup/wallpaper.sh

echo ""
echo "Installation done 😄 !"
