#!/bin/bash
echo "Installing git configuration ..."

if !( grep -Fq "nicolas@httpways.com" ~/.gitconfig ); then
	cat <<EOF >> ~/.gitconfig
[user]
	name = Nicolas AUCOURT
	email = nicolas@httpways.com
[color]
	diff = auto
	status = auto
	branch = auto
EOF
fi

echo "😄  done"

