#!/bin/bash

echo "Installing binaries files to your path"

if [ -d ~/.bin ] ; then
	rm -rfv ~/.bin
fi
mkdir -p ~/.bin
cat <<EOF >> ~/.profile

if [ -d ~/.bin  ] ; then
	PATH="$HOME/.bin:$PATH"
fi
EOF
ln -s ~/.dotfiles/.bin/* ~/.bin

echo "Installation of your binaires files done"

