#!/bin/bash
echo "Installing i3 configuration ..."

# Installaling Font Awesome Font
cd ~
wget -L "https://github.com/FortAwesome/Font-Awesome/archive/v4.7.0.zip"
unzip v4.7.0.zip && rm -fv v4.7.0.zip
cp -fv ~/Font-Awesome-4.7.0/fonts/fontawesome-webfont.ttf ~/.fonts/
rm -rfv ~/Font-Awesome-4.7.0

# Installaling Yosemite San Francisco Font
cd ~
wget -L "https://github.com/supermarin/YosemiteSanFranciscoFont/archive/master.zip"
unzip master.zip && rm -fv master.zip
cp -fv ~/YosemiteSanFranciscoFont-master/*.ttf ~/.fonts
rm -rfv ~/YosemiteSanFrancisco-master

# Installing LXAPPEARANCE
sudo apt-get install -y lxappearance

# Setting System San Francisco Display 12 font
sed -i "/gtk-font-name/cgtk-font-name=\"System San Francisco Display 12\"/" ~/.gtkrc-2.0
sed -i "/gtk-font-name/cgtk-font-name=\"System San francisco Display 12\"/" ~/.config/gtk-3.0/settings.ini

# Installation of Infinality
sudo add-apt-repository ppa:no1wantdthisname/ppa
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install -y fontconfig-infinality
# Configuring infinality
echo -e "3" | sudo bash /etc/fonts/infinality/infctl.sh setstyle
sudo sed -i "s/USE_STYLE=\"DEFAULT\"/USE_STYLE=\"UBUNTU\"/g" /etc/profile.d/infinality-settings.sh

# Installation of inconsolata font
cd ~/.fonts
wget -L "http://levien.com/type/myfonts/Inconsolata.otf"
cd ~

# Installation of RXVT UNICODE
sudo apt-get install -y rxvt-unicode-256color

# Installation of compton
sudo apt-get install -y compton

# Installation of NPM + Base16 builder
sudo apt-get install -y npm nodejs-legacy
sudo npm install --global base16-builder

echo "😄  done"

