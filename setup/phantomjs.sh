#!/bin/bash

echo "Installing phantomjs configuration ..."

# Installing PhantomJs
cd ~
wget "https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2"
tar -xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2
rm -fv phantomjs-2.1.1-linux-x86_64.tar.bz2
sudo cp -fv phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin
rm -rfv phantomjs-2.1.1-linux-x86_64/
phantomjs --version

echo " done"

