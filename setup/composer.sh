#!/bin/bash

EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
	>&2 echo 'ERROR: Invalid installer signature'
	rm composer-setup.php
	exit 1
fi

php composer-setup.php --quiet
RESULT=$?
rm -fv composer-setup.php
sudo cp -fv composer.phar /usr/local/bin/
rm -fv composer.phar
echo "alias composer='/usr/local/bin/composer.phar'" >> ~/.zshrc
exit $RESULT
