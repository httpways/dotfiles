#!/bin/bash
echo "Configuring Fstab for data partition..."

# FSTAB configuration
sudo cat <<EOF >> /etc/fstab
UUID=c0365c9d-8d65-4e0a-b3a6-68311d634579 /mnt/data ext4 errors=remount-ro 0 1
EOF

