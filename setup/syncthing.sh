#!/bin/bash

# Add the release PGP keys:
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -

# Add the "stable" channel to your APT sources:
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list

# Update and install syncthing:
sudo apt-get update
sudo apt-get install -y apt-transport-https syncthing python python3 python-pip python3-pip

mkdir -p ~/Sync/default
mkdir -p ~/Sync/adminShared

# Syncthing manager doc:
# https://github.com/classicsc/syncthingmanager
sudo -H pip3 install --upgrade pip
sudo -H pip3 install syncthingmanager

