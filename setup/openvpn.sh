#!/bin/bash
echo "Installing vpn for PV0 machine ..."

sudo apt-get install -y openvpn openssl

sudo mkdir -p /etc/openvpn/cert/pve0
sudo mkdir -p /etc/openvpn/cert/pve1
sudo mkdir -p /etc/openvpn/cert/pvez

sudo cp -fv ~/Sync/adminShared/VPN/PVE0/fw1-UDP4-61194-nick-laptop-ubuntu/*.{key,p12} /etc/openvpn/cert/pve0
sudo cp -fv ~/Sync/adminShared/VPN/PVE1/nick/fw1-UDP4-61194-nick-laptop-ubuntu/*.{key,p12} /etc/openvpn/cert/pve1
sudo cp -fv ~/Sync/adminShared/VPN/PVEZ/fw1-UDP4-61194-nick-laptop-ubuntu/*.{key,p12} /etc/openvpn/cert/pvez
sudo cp -fv ~/Sync/adminShared/VPN/templatesConfig/vpn-pve0.conf /etc/openvpn
sudo cp -fv ~/Sync/adminShared/VPN/templatesConfig/vpn-pve1.conf /etc/openvpn
sudo cp -fv ~/Sync/adminShared/VPN/templatesConfig/vpn-pveZ.conf /etc/openvpn

sudo sed -i 's/#AUTOSTART="all"/AUTOSTART="all"/g' /etc/default/openvpn

sudo systemctl daemon-reload

sudo /etc/init.d/openvpn restart

