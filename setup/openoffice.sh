#!/bin/bash
echo "Installing Openoffice ..."

# Downloading Openoffice
wget https://downloads.sourceforge.net/project/openofficeorg.mirror/4.1.3/binaries/fr/Apache_OpenOffice_4.1.3_Linux_x86-64_install-deb_fr.tar.gz

tar -xvf Apache_OpenOffice_*.tar.gz
rm -fv Apache_OpenOffice_*.tar.gz
sudo dpkg -i fr/DEBS/*.deb
sudo dpkg -i fr/DEBS/desktop-integration/*.deb

rm -rfv fr

echo "Installation done"

