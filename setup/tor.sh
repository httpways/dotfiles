#!/bin/bash

# Add the release PGP keys:
sudo add-apt-repository ppa:nerdherd/cloud

# Update and install syncthing:
sudo apt-get update
sudo apt-get install -y tor-browser-fr64

