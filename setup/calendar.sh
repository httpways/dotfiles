#!/bin/bash
echo "Configuring Caldav & cardav client..."

sudo apt-get install -y python python-pip python3 python3-pip python-pysqlite2 python-pysqlite2-doc python-dev python3-dev python-virtualenv python3-virtualenv virtualenv virtualenv-clone virtualenvwrapper libxml2 libxml2-dev libxslt1.1 libxslt-dev zlib1g libssl-dev

# Vdirsyncer installation
sudo -H pip3 install vdirsyncer

# Configuration directory creation
mkdir -p /home/nick/.config/vdirsyncer
mkdir -p /home/nick/.vdirsyncer/status/

# Vdirsyncer configuration
cat <<EOF > /home/nick/.config/vdirsyncer/config
[general]
status_path = ~/.vdirsyncer/status

EOF

# Khal installation
sudo -H pip3 install git+git://github.com/pimutils/khal.git

# Khal configuration
cat <<EOF > /home/nick/.config/khal/config
[calendars]
[[PERSO]]

EOF
