#!/bin/bash
echo "Configuring POWERLINE ..."

# Depedencies installation
sudo apt-get install -y build-essential gcc g++ wget socat libffi-dev cython cython3 cython-doc

# Installing the latest version of libgit2
cd ~
wget https://github.com/libgit2/libgit2/archive/v0.25.0.tar.gz
tar xzf v0.25.0.tar.gz
cd libgit2-0.25.0/
cmake .
make
sudo make install
cd ~
rm -rfv ~/libgit2-0.25.0 && rm -fv v0.25.0.tar.gz

# Python dependencies packages
sudo -H pip3 install psutil pygit2 pyuv i3ipc

# POWERLINE installation
sudo -H pip3 install powerline-status

# Not necessary for the moment
# sudo apt-get install powerline python-powerline python3-powerline fonts-powerline python-powerline-doc

# Installation of patched fonts
git clone https://github.com/powerline/fonts.git
cd fonts
sed -i "s/.local\/share\/fonts/.fonts/g" ./install.sh
./install.sh 
cd .. && rm -rf fonts
