#!/bin/bash

# Install PHP Packages:
sudo apt-get install -y php php-xml php-fdomdocument php-mcrypt php-json php-intl php-mysql php-pgsql php-sqlite3 php-apcu php-pear php-dev php-gd php-soap php-curl

# Enabling APC fallback for PHP5.6
sudo pecl install -f apcu_bc
sudo cat <<EOF >/etc/php/7.0/mods-available/apc.so
extension=apc.so
EOF
sudo ln -s /etc/php/7.0/mods-available/apc.so /etc/php/7.0/cli/conf.d/21-apc.ini


# Setting up timezone
sed -i "/;date.timezone =/adate.timezone = 'Europe/Paris'" /etc/php/7.0/cli/php.ini
sed -i "/;date.timezone =/adate.timezone = 'Europe/Paris'" /etc/php/7.0/fpm/php.ini


