#!/bin/bash

echo "Installing casperjs configuration ..."

# Installing CasperJs
cd ~/.bin
git clone git://github.com/casperjs/casperjs.git
sudo ln -sf $PWD/casperjs/bin/casperjs /usr/local/bin/casperjs
casperjs --version

echo " done"

