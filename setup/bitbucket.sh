#!/bin/bash
echo "Installing bitbucket configuration ..."

if [ ! -f ~/.ssh/nick-laptop-ubuntu_rsa ]; then
	ssh-keygen -b 4096 -N "" -f ~/.ssh/nick-laptop-ubuntu_rsa
fi

if !( grep -Fq "bitbucket.org" ~/.ssh/config )
then
cat <<EOF >> ~/.ssh/config

Host bitbucket.org
IdentityFile ~/.ssh/nick-laptop-ubuntu_rsa
IdentitiesOnly yes
Port 22
EOF
fi

echo "😄  done"

