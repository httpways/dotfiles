#!/bin/bash
echo "Configuring ZSH ..."

# ZSH installation
sudo apt-get install -y zsh zsh-common zsh-doc 

# lolcat,screenfetch & figlet installation
sudo apt-get install -y lolcat figlet screenfetch fortune
lolcat --version

# Installation tmux, tmuxinator
sudo apt-get install -y tmux tmuxinator

# Installation of interactive filter + jq + peco + Emojify 
sudo apt-get install jq
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
sudo curl -L https://github.com/peco/peco/releases/download/v0.5.1/peco_linux_amd64.tar.gz > ~/peco.tar
tar xvf ~/peco.tar && rm -fv ~/peco.tar
sudo cp -fv ~/peco_linux_amd64/peco /usr/local/bin && rm -rfv ~/peco_linux_amd64
sudo sh -c "curl https://raw.githubusercontent.com/mrowa44/emojify/master/emojify -o /usr/local/bin/emojify && chmod +x /usr/local/bin/emojify"
sed -i "s/\^s/\E/g" ~/.zplug/repos/b4b4r07/emoji-cli/emoji-cli.zsh

# Changing default shell
echo -e "nic0l@s\n$(which zsh)\n" | chsh

# Zshenv update
cat <<EOF > /home/nick/.zshenv
screenfetch | lolcat -F 0.3 && 
( espeak -s 125 -v en+f5 "Welcome \$USER, your new Terminal is ready" & )
figlet Welcome \$USER | lolcat -a -F 0.3 -d 10
EOF

# Zlogin update
cat <<EOF > /home/nick/.zlogin
( espeak -s 125 -v en+f5 "Hello \$USER, your new session is opening" & )
figlet Hello \$USER | lolcat -a -F 0.3 -d 10
EOF

# Zlogout update
cat <<EOF > /home/nick/.zlogout
( espeak -s 125 -v en+f5 "Bye bye \$USER, your session will close" & )
figlet See you later \$USER | lolcat -a -F 0.3 -d 10
EOF


# Imagemagik + catimg installation
sudo apt-get install -y imagemagick imagemagick-6.q16 imagemagick-common cmake make
cd ~
git clone https://github.com/posva/catimg.git
cd ~/catimg && cmake . && sudo make install
cd ~
rm -rfv ~/catimg

# installation if FZY
wget https://github.com/jhawthorn/fzy/releases/download/0.8/fzy_0.8-1_amd64.deb
sudo dpkg -i fzy_0.8-1_amd64.deb
rm -fv fzy_0.8-1_amd64.deb

# Installation of Percol
sudo -H pip install percol

# Installation of ZPLUG
export ZPLUG_HOME=/home/nick/.zplug
git clone https://github.com/zplug/zplug $ZPLUG_HOME
cat >> ~/.zshrc <<EOF
source ~/.zplug/init.zsh
zplug "zsh-users/zsh-syntax-highlighting"
zplug "kennethreitz/autoenv"
EOF
zplug install

    apache2-macports
    catimg
    celery
    chucknorris
    composer
    coffee
    common-aliases
    compleat
    debian
    dircycle
    django
    encode64
    git
    git-extras
    git-flow
    git-flow-avh
    git-hubflow
    git_remote_granch
    httpie
    history
    jsontools
    lol
    mysql-macports
    ng
    node
    npm
    perms
    phing
    pip
    pyenv
    pylint
    python
    rand-quote
    redis-cli
    repo
    ssh-agent
    supervisor
    symfony2
    systemd
    thor
    tmux
    tmuxinator
    urltools
    vi-mode
    web-search
    colored-man-pages
    b4b4r07/emoji-cli
