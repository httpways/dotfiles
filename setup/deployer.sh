#!/bin/bash

curl -LO https://deployer.org/deployer.phar
sudo mv deployer.phar /usr/local/bin/dep
chmod +x /usr/local/bin/dep

curl -LO https://deployer.org/releases/v3.3.0/deployer.phar
sudo mv deployer.phar /usr/local/bin/dep3
chmod +x /usr/local/bin/dep3
