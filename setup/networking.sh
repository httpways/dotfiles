#!/bin/bash
echo "Configuring network..."

sudo apt-get install -y dnsutils network-manager

# HOSTNAME
sudo bash -c 'cat <<EOF > /etc/hostname
nick-laptop-ubuntu
EOF'

# DNS MASQ
sudo sed -i "s/dns=dnsmasq/#dns=dnsmasq/g" /etc/NetwokManager/NetworkManager.conf
sudo update-rc.d -f dnsmasq remove
sudo service network-manager restart

NETWORK="Auto Livebox-c172"

nmcli con show $NETWORK
#nmcli con mod "$NETWORK" ipv4.dns "192.168.50.1 208.67.222.222 208.67.220.220" ipv4.ignore-auto-dns yes
nmcli con mod "$NETWORK" ipv4.dns "192.168.50.1" ipv4.ignore-auto-dns yes
nmcli con down $NETWORK
nmcli con up $NETWORK
