#!/bin/bash
echo "Installing Gitlab configuration ..."

if [ ! -f ~/.ssh/nick-laptop-ubuntu_rsa ]; then
	ssh-keygen -b 4096 -N "" -f ~/.ssh/nick-laptop-ubuntu_rsa
fi

if !( grep -Fq "git.pve0.httpways.org" ~/.ssh/config )
then
cat <<EOF >> ~/.ssh/config

Host git.pve0.httpways.org
IdentityFile ~/.ssh/nick-laptop-ubuntu_rsa
IdentitiesOnly yes
Port 22
EOF
fi

echo "😄  done"

