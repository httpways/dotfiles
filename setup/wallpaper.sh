#!/bin/bash
echo "Installing wallpaper configuration ..."

mkdir -p ~/Images/wallpapers
curl http://static.simpledesktops.com/uploads/desktops/2015/06/15/ramadan-2015-wallpaper1.png -o ~/Images/wallpapers/1.png
curl http://static.simpledesktops.com/uploads/desktops/2015/07/11/Yosemite-Color-Block.png -o ~/Images/wallpapers/2.png
curl http://static.simpledesktops.com/uploads/desktops/2015/07/14/polygonal-shape-4k.png -o ~/Images/wallpapers/3.png
curl http://static.simpledesktops.com/uploads/desktops/2015/07/07/2880x1800.png -o ~/Images/wallpapers/4.png
curl http://static.simpledesktops.com/uploads/desktops/2015/07/29/Glitch.png -o ~/Images/wallpapers/5.png
curl http://static.simpledesktops.com/uploads/desktops/2015/08/15/oragami_s.png -o ~/Images/wallpapers/6.png
curl http://static.simpledesktops.com/uploads/desktops/2015/09/25/Siri.png -o ~/Images/wallpapers/7.png
curl http://static.simpledesktops.com/uploads/desktops/2015/10/18/hoverboard_simple.png -o ~/Images/wallpapers/8.png
curl http://static.simpledesktops.com/uploads/desktops/2015/12/28/strider.png -o ~/Images/wallpapers/9.png
curl http://static.simpledesktops.com/uploads/desktops/2016/02/08/moonRising.png -o ~/Images/wallpapers/10.png
curl http://static.simpledesktops.com/uploads/desktops/2016/02/08/sunRising.png -o ~/Images/wallpapers/11.png
curl http://static.simpledesktops.com/uploads/desktops/2016/08/28/Wind-Vector-resize.png -o ~/Images/wallpapers/12.png
curl http://static.simpledesktops.com/uploads/desktops/2016/07/19/Path.png -o ~/Images/wallpapers/13.png
curl http://static.simpledesktops.com/uploads/desktops/2016/10/12/Hydrogen_Remixed.png -o ~/Images/wallpapers/14.png
curl http://static.simpledesktops.com/uploads/desktops/2016/07/07/b3d9be-background.png -o ~/Images/wallpapers/15.png
curl http://static.simpledesktops.com/uploads/desktops/2015/02/25/Lattice.png -o ~/Images/wallpapers/16.png
curl http://static.simpledesktops.com/uploads/desktops/2015/02/20/zentree_1.png -o ~/Images/wallpapers/17.png
curl http://static.simpledesktops.com/uploads/desktops/2014/10/22/Buzz.png -o ~/Images/wallpapers/18.png
curl http://static.simpledesktops.com/uploads/desktops/2014/10/28/Yosemite.png -o ~/Images/wallpapers/19.png
curl http://static.simpledesktops.com/uploads/desktops/2013/07/11/squares_widew.png -o ~/Images/wallpapers/20.png
curl http://static.simpledesktops.com/uploads/desktops/2013/07/30/kontrols2-2880x1800.png -o ~/Images/wallpapers/21.png

echo "😄  done"

